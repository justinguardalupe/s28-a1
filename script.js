const url = "https://jsonplaceholder.typicode.com/todos/";

// ==================================> #3
fetch(url).then((response) => response.json()).then((json) => console.log(json));	

// ==================================> #4
fetch(url).then((response) => response.json()).then((json) => console.log(json.map(items => items.title)));

// ==================================> #5
fetch(`${url}/1`).then((response) => response.json()).then((json) => console.log(json));

// ==================================> #6
fetch(`${url}/1`).then((response) => response.json()).then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}.`));

// ==================================> #7
fetch(url, {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		userID: 1,
		id: 201,
		title: 'Created To Do List Item',
		completed: false
	})
}).then((response) => response.json()).then((json) => console.log(json));

// ==================================> #8
fetch(`${url}/1`, {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		userID: 1,
		id: 201,
		title: 'Updated To Do List Item',
		completed: false
	})
}).then((response) => response.json()).then((json) => console.log(json));

// ==================================> #10
fetch(`${url}/1`, {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
		description: 'To update the my to do list with a different data structure',
		status: 'Pending',
		dateCompleted: 'Pending',
		userId: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));

// ==================================> #11
fetch(`${url}/1`, {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'delectus aut autem',
		completed: false,
		status: 'Complete',
		dateCompleted: '07/09/21',
		userId: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));

// ==================================> #12
fetch(`${url}/1`, { method: 'DELETE' });
